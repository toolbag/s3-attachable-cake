<?php

class File extends AppModel {
  public $name = 'File';
  public $displayField = 'title';
  public $actsAs = array(
    'S3Attachable' => array(
      'logging' => true, // whether logging should be performed, if TRUE writes to 'app/tmp/logs/s3attachable.log'
      's3_options' => array(
        'acl' => 'public', // One of: public, private
        'key_format' => ':id/:filename'
      ),
      'types' => array(
        'xls' => array(
          'application/msexcel',
          'application/vnd.ms-excel',
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ),
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'xlsx' => array('application/msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
      )
    )
  );
  
  /*
  Supported fields are:
  
    :id -> ID of the model object
    :filename -> name of the filename uploaded by the client
  */
  public function s3_key($options = array()) {
    $format = $this->actsAs['S3Attachable']['s3_options']['key_format'];
    $key = $format;
    if(strpos($format, ':id') !== FALSE) {
      $key = str_replace(':id', $options['id'], $key);
    }
    if(array_key_exists('filename', $options)) {
      $key = str_replace(':filename', $options['filename'], $key);
    }
    return $key;
  }
  
  
}

?>
