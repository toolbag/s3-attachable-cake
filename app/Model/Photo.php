<?php

class Photo extends AppModel {
  public $name = 'Photo';
  public $displayField = 'title';
  public $actsAs = array(
    'S3Attachable' => array(
      'logging' => true, // whether logging should be performed, if TRUE writes to 'app/tmp/logs/s3attachable.log'
      'thumbnails' => array(
        'small' => '88x82',
        'medium' => '140x156',
        'large' => '1024x768'
      ),
      's3_options' => array(
        'acl' => 'public', // One of: public, private
        'key_format' => ':id/:size/:filename' // :size is thumbnail string (88x82), NOT the file size
      ),
      'types' => array(
        'jpg' => array('image/jpg', 'image/jpeg', 'image/pjpeg'),
        'jpeg' => array('image/jpeg', 'image/pjpeg'),
        'png' => 'image/png',
        'gif' => 'image/gif'
      ),
      'autocrop' => array('small', 'medium'),
      'dontUpscale' => array('large')
    )
  );
  
  /*
  Supported fields are:
  
    :id -> ID of the model object
    :filename -> name of the filename uploaded by the client
    :size -> thumbnail size
  */
  public function s3_key($options = array()) {
    $format = $this->actsAs['S3Attachable']['s3_options']['key_format'];
    $key = $format;
    if(strpos($format, ':id') !== FALSE) {
      $key = str_replace(':id', $options['id'], $key);
    }
    if(array_key_exists('size', $options)) {
      $key = str_replace(':size', $options['size'], $key);
    }
    if(array_key_exists('filename', $options)) {
      $key = str_replace(':filename', $options['filename'], $key);
    }
    return $key;
  }

}

?>
