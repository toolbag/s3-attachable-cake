<?php

App::import('Vendor','aws-sdk', array('file' => 'aws/sdk.class.php'));
App::uses("ImageManipulation", "Lib");

class S3AttachableBehavior extends ModelBehavior {
  public $_defaults = array(
    'label' => 'attachment',
    'errorMessages' => array(
      'isUploaded' => 'Malicious file upload.',
      'moveUploaded' => 'Error moving file upload.',
      'isValidType' => 'Invalid file type. Please check your file type and try again.',
      'isValidSize' => 'Invalid file size. Please check your file size and try again.'
    ),
    'thumbQuality' => 80,
    'maxSize' => 1048576,
    'fields' => array(
      'attachment_width'    => 'attachment_width',
      'attachment_height'   => 'attachment_height',
      'attachment_name'     => 'attachment_name',
      'attachment_type'     => 'attachment_type',
      'attachment_size'     => 'attachment_size'
    )
  );

  public $_validThumbTypes = array('image/jpg', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/gif');
  public $_errorMsg = "Error uploading file";

  public $settings = array();
  public $model = null;
  
  private $fields;
  private $types;
  private $s3_options;
  private $log_enabled;
  private $performed_attach_actions = false;

  function setup(Model $model, $config = array()) {
    $this->settings[$model->name] = array_merge($this->_defaults, $config);
    $this->model = $model;
    $this->fields = $this->settings[$model->name]['fields'];
    $this->types = $this->settings[$model->name]['types'];
    $this->s3_options = $this->settings[$model->name]['s3_options'];
    $this->log_enabled = $this->settings[$model->name]['logging'];
    
    // Verify S3 is valid
    $bucket = Configure::read('aws.bucket', NULL);
    $key = Configure::read('aws.key', NULL);
    $secret = Configure::read('aws.secret', NULL);
    if($bucket == NULL || $key == NULL || $secret == NULL) {
      die('S3 configuration missing: fix it in core.php');
    }
  }
  
  /* Used by the Helper to determine if it needs to generate signed URLs or note */
  public function is_s3_private() {
    return $this->s3_options['acl'] == 'private';
  }
  
  function beforeDelete($model) {
    $bucket = Configure::read('aws.bucket');
    $attachment_name_field = $this->fields['attachment_name'];
    $attachment_name = $model->data[$model->name][$attachment_name_field];

    // delete thumbnails if this type supports them
    if(isset($this->settings[$model->name]['thumbnails'])) {
      // first delete the original
      $key = $model->s3_key(
        array(
          'filename' => $attachment_name,
          'size' => 'original',
          'id' => $model->id
        )
      );
      $this->deleteFileFromS3($bucket, $key);
      // delete all thumbnails
      foreach ($this->settings[$model->name]['thumbnails'] as $dir => $size) {
        $key = $model->s3_key(
          array('filename' => $attachment_name, 'size' => $size, 'id' => $model->id)
        );
        $this->deleteFileFromS3($bucket, $key);
      }
    } else {
      // single document type, delete just itself
      $key = $model->s3_key(
        array(
          'filename' => $attachment_name,
          'id' => $model->id
        )
      );
      $this->deleteFileFromS3($bucket, $key);
    }
    
    return true;
  }
  
  /* Validate the attachment */
  function beforeSave($model) {
    $label = $this->settings[$model->name]['label'];
    if(isset($model->data[$model->name][$label])) {
      $attachment = $model->data[$model->name][$label];

      $is_uploaded = $this->_isUploaded($attachment);
      $valid_size = $this->_isValidSize($attachment['size']);
      $valid_type = $this->_isValidType($attachment);
      if (!$is_uploaded || !$valid_size || !$valid_type) {
        $this->logger("Attachment not saved. ".$this->_errorMsg,'s3attachable');
        $model->validationErrors[$label] = $this->_errorMsg;
        return false;
      }
    }
    return true;
  }

  function afterSave($model) {
    if($this->performed_attach_actions == FALSE) {
      $label = $this->settings[$model->name]['label'];
      if(isset($model->data[$model->name][$label])) {
        $attachment = $model->data[$model->name][$label];

        $extension = pathinfo($attachment['name'], PATHINFO_EXTENSION);
        if(!empty($attachment['name'])) {

          $attachment_name = $this->fields['attachment_name'];

          $model->data[$model->name][$attachment_name] = $attachment['name'];
          $model->data[$model->name][$this->fields['attachment_type']] = $attachment['type'];
          $model->data[$model->name][$this->fields['attachment_size']] = $attachment['size'];

          $s3_acl = NULL;
          if($this->s3_options['acl'] == 'public') {
            $s3_acl = AmazonS3::ACL_PUBLIC;
          } elseif($this->s3_options['acl'] == 'private') {
            $s3_acl = AmazonS3::ACL_PRIVATE;
          }

          $bucket = Configure::read('aws.bucket');
          if(empty($bucket)) {
            throw new Exception("Missing S3 bucket: in core.php specify Configure::write('aws.bucket', '??')");
          }

          // Upload original to S3, but determine if this
          // is a single document type (e.g. XLS) or has thumbnails
          $path_to_original = $attachment['tmp_name'];
          // These S3 options will be re-used for the original & the thumbnails
          $s3_options = array(
            'acl' => $s3_acl,
            'contentType' => $attachment['type']
          );
          $s3_key_format = $this->s3_options['key_format'];

          if(isset($this->settings[$model->name]['thumbnails'])) {

            $key = $model->s3_key(
              array(
                'filename' => $attachment['name'], 
                'size' => 'original',
                'id' => $model->id
              )
            );
            $result = $this->uploadFileToS3($path_to_original, $bucket, $key, $s3_options);

            $this->logger("Creating thumbs "  .print_r($attachment,true),'s3attachable');
            if (in_array($attachment['type'], $this->_validThumbTypes)) {
              list($width, $height) = getimagesize($attachment['tmp_name']);

              // store properties for the original image
              $model->data[$model->name][$this->fields['attachment_width']] = $width;
              $model->data[$model->name][$this->fields['attachment_height']] = $height;
              
              if(isset($this->settings[$model->name]['thumbnails'])) {
                $manipulator = new ImageManipulation();
                foreach ($this->settings[$model->name]['thumbnails'] as $dir => $size) {
                  list($width, $height) = explode('x', strtolower($size));

                  $output_file = tempnam(sys_get_temp_dir(), $size);
                  $autocrop = in_array($dir, $this->settings[$this->model->name]['autocrop']) == true;
                  $dontUpscale = in_array($dir, $this->settings[$this->model->name]['dontUpscale']) == true;
                  $result = $manipulator->resizeToThumb(
                    $path_to_original,
                    $output_file,
                    (int)$width,
                    (int)$height,
                    array(
                      'kind' => $extension,
                      'quality' => $this->settings[$this->model->name]['thumbQuality'],
                      'autocrop' => (isset($this->settings[$this->model->name]['autocrop'])) && $autocrop,
                      'dontUpscale' => (isset($this->settings[$this->model->name]['dontUpscale']) && $dontUpscale)
                    )
                  );
                  $key = $model->s3_key(
                    array('filename' => $attachment['name'], 'size' => $size, 'id' => $model->id)
                  );
                  $this->logger("Scaled thumb: $size, output-file=" . $output_file . ', s3-key: ' . $key,'s3attachable');
                  $result = $this->uploadFileToS3($output_file, $bucket, $key, $s3_options);
                  if(file_exists($output_file)) {
                    @unlink($output_file);
                  }
                }
              }
            }
          } else {
            // its a single document type, so upload just the original file
            $key = $model->s3_key(array('filename' => $attachment['name'], 'id' => $model->id));
            $result = $this->uploadFileToS3($path_to_original, $bucket, $key, $s3_options);
          }
        }
      } 

      $this->performed_attach_actions = TRUE;

      // finally save the model again to persist the attributes we just calculated
      $model->save();
    } // performed_attach_actions == FALSE
    return true;
  }

  private function _isUploaded($attachment) {
    $no_error_code = isset($attachment['error']) && $attachment['error'] == 0;
    $none_tmp_name = !empty($attachment['tmp_name']) && $attachment['tmp_name'] != 'none';
    if($no_error_code || $none_tmp_name) {
      return true;
    } else {
      $this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isUploaded'];
      return false;
    }
  }

  private function _isValidType($attachment) {
    $ext = pathinfo($attachment['name'], PATHINFO_EXTENSION);
    if (isset($this->settings[$this->model->name]['types'])) {
      $extensions = $this->settings[$this->model->name]['types'];
      $valid_types = array_keys($extensions);
      if(in_array($ext, $valid_types)) {
        return true;
      }
    }
    $this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isValidType'];
    return false;
  }

  private function _isValidSize($size) {
    if($size == 0) {
      return false;
    }
    if ($size <= $this->settings[$this->model->name]['maxSize']) {
      return true;
    }
    $this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isValidSize'];
    return false;
  }

  /**
  * Upload a file to a s3 bucket
  */
  private function uploadFileToS3($filepath, $bucket, $key, $options = array()) {
    $options = array_merge(array('removeFile' => false), $options);
    $options['fileUpload'] = $filepath;

    $s3 = new AmazonS3();

    $response = $s3->create_mpu_object($bucket, $key, $options);

    // Remove original file in server
    if ($options['removeFile'] == true) {
      unlink($filePath);
    }
    return $response->isOK();
  }

  /**
  * delete a file from a bucket
  */
  private function deleteFileFromS3($bucket, $key) {
    $s3 = new AmazonS3();
    $response = $s3->delete_object($bucket, $key);

    $result = $response->isOK();

    if ($result) {
      $this->logger("OK - $key deleted from $bucket");
    } else {
      $this->logger("FAIL - error deleting $key from $bucket");
    }
    return $response->isOK();
  }

  private function logger($msg) {
    if($this->log_enabled == TRUE) {
      $prefix = $this->model->name . '(' . $this->model->id . ')';
      $this->log($prefix . ' ' . $msg, 's3attachable');
    }
  }

  /**
    Takes a MIME type such as 'image/jpeg' and returns the file extension mapping to it: 'jpg'
    Uses the type map defined in the calling Model class (e.g. Photo)
  */
  private function mime_type_to_extension($mime) {
    foreach($this->types AS $extension => $value) {
      if(is_array($value)) {
        if(in_array($mime, $value)) {
          return $extension;
        }
      } elseif(is_string($value)) {
        if($mime == $value) {
          return $extension;
        }
      }
    }
    return NULL;
  }

}
?>
