<?php

class PhotosController extends AppController {
    public $name = 'Photos';
    public $components = array();
    public $uses = array();
    public $helpers = array('Html', 'Form');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function index() {
        $photos = $this->Photo->find('all', array('order' => array('id' => 'desc')));
        $this->set('photos', $photos);
    }

    public function view($id) {
      $photo = $this->Photo->read(null, $id);
      $this->set('photo', $photo);
    }
    
    public function add() {
      if(!empty($this->request->data)) {
        $this->Photo->create();
        if($this->Photo->save($this->request->data)) {
          $this->Session->setFlash('The photo has been saved');
          $this->redirect(array('action' => 'index'));
        } else {
          $this->Session->setFlash('The photo could not be saved. Please, try again.');
        }
      }
    }
    
    public function delete($id = null) {
      $photo = $this->Photo->read(null, $id);
      if ($this->Photo->delete($id)) {
        $this->Session->setFlash('Photo deleted!');
      }
      $this->redirect(array('action' => 'index'));
    }


}