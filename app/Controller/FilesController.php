<?php

class FilesController extends AppController {
    public $name = 'Files';
    public $components = array();
    public $uses = array();
    public $helpers = array('Html', 'Form');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function index() {
        $files = $this->File->find('all', array('order' => array('id' => 'desc')));
        $this->set('files', $files);
    }

    public function view($id) {
      $file = $this->File->read(null, $id);
      $this->set('file', $file);
    }
    
    public function add() {
      if(!empty($this->request->data)) {
        $this->File->create();
        if($this->File->save($this->request->data)) {
          $this->Session->setFlash('The File has been saved');
          $this->redirect(array('action' => 'index'));
        } else {
          $this->Session->setFlash('The File could not be saved. Please, try again.');
        }
      }
    }
    
    public function delete($id = null) {
      $file = $this->File->read(null, $id);
      if ($this->File->delete($id)) {
        $this->Session->setFlash('File deleted!');
      }
      $this->redirect(array('action' => 'index'));
    }    

}