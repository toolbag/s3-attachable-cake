<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
  
  public function url_to_s3_object($model_name, $properties, $use_https = false) {
    $model = ClassRegistry::getObject($model_name);
    $key = $model->s3_key($properties);
    $bucket = Configure::read('aws.bucket');
    $url = ('http' . ($use_https ? 's' : '') . '://' . $bucket . '.s3.amazonaws.com/' . $key);
    if($model->is_s3_private()) {
      return($this->get_signed_s3_url($bucket, $key));
    } else {
      return($url);
    }
  }
  
  private function get_signed_s3_url($bucket, $key, $options = array()) {
    $options = array_merge(array('preauth' => '10 minutes', 'opt' => array()), $options);
    $s3 = new AmazonS3();
    $response = $s3->get_object_url($bucket, $key, $options['preauth'],$options['opt']);
    return $response;
  }
}
