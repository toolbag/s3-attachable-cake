<h1>Upload New File</h1>
<?php
    echo $this->Form->create('File', array('action' => 'add', 'enctype' => 'multipart/form-data'));
    echo $this->Form->input('title');
    echo $this->Form->input('description');
    echo $this->Form->input('attachment', array('type' => 'file'));
    echo $this->Form->end('Save');