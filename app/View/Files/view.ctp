<?php

echo "Created: " . $file['File']['created'] . "<br />";
echo "Title: " . $file['File']['title'] . '<br />';
echo "Description: " . $file['File']['description'] . '<br />';

?>

<p>Download file:</p>
<?php
  $properties = $file['File'];
  $properties['filename'] = $file['File']['attachment_name'];
?>
<a href="<?php echo $this->App->url_to_s3_object('File', $properties); ?>"><?php echo $file['File']['attachment_name']; ?></a>

<hr />
<br /><br />
<?php
$delete_file_url = array('controller' => 'files', 'action' => 'delete', $file['File']['id']);
echo $this->Html->link('Delete File', $delete_file_url);
?>
