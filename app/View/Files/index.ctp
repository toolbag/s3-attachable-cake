<?php
$new_file_url = array('controller' => 'files', 'action' => 'add');
echo '&raquo; ' . $this->Html->link('New File', $new_file_url);
?>
<h3>Files</h3>
<?php
  echo "<div><ul>";
  foreach($files AS $file) {
    echo "<li>";
    echo "Created: " . $file['File']['created'] . "<br />";
    echo "Title: " . $file['File']['title'] . '<br />';
    echo "Description: " . $file['File']['description'] . '<br />';
    $file_url = array('controller' => 'files', 'action' => 'view', $file['File']['id']);
    echo '&raquo; ' . $this->Html->link('View', $file_url);
    echo "</li>";
  }
  echo "</ul></div>";
?>