<?php

echo "Created: " . $photo['Photo']['created'] . "<br />";
echo "Title: " . $photo['Photo']['title'] . '<br />';
echo "Description: " . $photo['Photo']['description'] . '<br />';

?>

<p>original</p>
<?php
  $properties = $photo['Photo'];
  $properties['size'] = 'original';
  $properties['filename'] = $photo['Photo']['attachment_name'];
?>
<img src="<?php echo $this->App->url_to_s3_object('Photo', $properties); ?>" />

<hr />

<p>140x156</p>
<?php
  $properties = $photo['Photo'];
  $properties['size'] = '140x156';
  $properties['filename'] = $photo['Photo']['attachment_name'];
  $url = $this->App->url_to_s3_object('Photo', $properties);
?>
<img src="<?php echo $url; ?>" />

<hr />
<?php
$delete_photo_url = array('controller' => 'photos', 'action' => 'delete', $photo['Photo']['id']);
echo $this->Html->link('Delete Photo', $delete_photo_url);
?>
