<h1>Upload New Photo</h1>
<?php
    echo $this->Form->create('Photo', array('action' => 'add', 'enctype' => 'multipart/form-data'));
    echo $this->Form->input('title');
    echo $this->Form->input('description');
    echo $this->Form->input('attachment', array('type' => 'file'));
    echo $this->Form->end('Save');