<?php
$new_photo_url = array('controller' => 'photos', 'action' => 'add');
echo '&raquo; ' . $this->Html->link('New Photo', $new_photo_url);
?>
<h3>Photos</h3>
<?php
  echo "<div><ul>";
  foreach($photos AS $photo) {
    echo "<li>";
    echo "Created: " . $photo['Photo']['created'] . "<br />";
    echo "Title: " . $photo['Photo']['title'] . '<br />';
    echo "Description: " . $photo['Photo']['description'] . '<br />';
    $photo_url = array('controller' => 'photos', 'action' => 'view', $photo['Photo']['id']);
    echo '&raquo; ' . $this->Html->link('View', $photo_url);
    echo "</li>";
  }
  echo "</ul></div>";
?>