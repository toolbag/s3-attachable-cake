S3 Attachable App
=================

1. Open `core.php` and at the bottom configure AWS.

2. Create the database and load tables:

```
  mysql> create database s3_attachable_proto;
```

```
$ mysql -u root s3_attachable_proto < db/s3_attachable_proto.sql
```

There are two models which implement the behavior: `Photo` and `File`.

`Photo` will do thumbnailing - notice how in the `$actsAs` definition in the `s3_options` there is a flexible key naming. The `File` model implements
a different S3 key name. The core of the S3 key generation logic is in the `s3_key` method which must be implemented in the Model.

`File` will not do thumbnailing so it has a single S3 key. The determination of whether to do thumbnailing is based on the presence of `thumbnails`
in the `$actsAs` configuration.

If you go to `/files` and `/photos` you can upload a file of each type and then drill down into an existing record. Go ahead and upload a Photo and then view
it, you'll see I dump out 2 different sizes. The S3 key generation for the file link is done in the `AppHelper`.

Changes from the LoC behavior:

1. Overall cleanup, my behavior is 300 lines of PHP vs 500 in LoC.
2. I was able to clean up the Behavior from LoC quite a bit by removing any local upload stuff. LoC takes the temporary file that we get from Apache, moves
it to a temporary file under the webroot and then uploading those to S3. My behavior operates on the single already existing temp file from Apache
and then uses the OS temp file location to store the thumbnails during the uploading.


IMPORTANT
=========

I had to adjust `Lib/ImageManipulation.php` - before it assumed the incoming file looked like `foo.jpg` (a file with an extension) and it parsed
that extension to determine whether `imagejpeg`, `imagegif`, etc were used. When I ditched the double copying of files and just moved to operating
on the single temp upload file from the webserver we lost the original name (its in the metadata but the actual file handle is a gibberish one). Thus I had
to adjust the `resizeToThumb` to take a parameter of the type to operate in. Thus the caller of the method can dictate what the type is and it doesn't try and
get smart and figure it out for itself. But of course that method is still supported, if the caller doesnt specify it.

THUS YOU WILL NEED TO TAKE MY UPDATED `Lib/ImageManipulation.php` and not use any other one.
